import os

# Fonction pour se connecter au serveur GitLab
def git_pull():
    gitlab_url = "gitlab.com"
    ssh_dir = os.path.expanduser("~/.ssh")
    # Vérifier si le répertoire .ssh existe
    if not os.path.exists(ssh_dir):
        os.makedirs(ssh_dir)
        os.chmod(ssh_dir, 0o700)
    # Ajouter la clé publique du serveur GitLab
    os.system(f'ssh-keyscan {gitlab_url} >> {ssh_dir}/known_hosts')
    # Définir la variable GIT_SSH_COMMAND pour utiliser la clé privée de l'utilisateur
    os.environ['GIT_SSH_COMMAND'] = 'ssh -i /home/semauser/.ssh/id_rsa'

    # Cloner le référentiel
    gitlab_repo = "salmanwk34/mspr_dev.git"
    os.system(f'git -C /home/semauser/.local/lib/python3.10/site-packages pull git@{gitlab_url}:{gitlab_repo}')

# Mettre à jour la machine Ubuntu
def update_system():
    os.system('sudo apt update && sudo apt upgrade -y')

if __name__ == '__main__':
    # Mettre à jour la machine Ubuntu au démarrage
    update_system()
    # Se connecter automatiquement au serveur GitLab et cloner le référentiel
    git_pull()
