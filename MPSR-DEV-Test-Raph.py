import dash
from dash import html
from dash import dcc
import pandas as pd
import nmap
import socket
import ipaddress
import requests

# Fonction pour récupérer l'adresse IP locale
def adresse_ip_recuperation():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    local_ip = s.getsockname()[0]
    s.close()
    network = ipaddress.ip_network(f"{local_ip}/24", strict=False)
    return str(network)

# Fonction de recueil de l'adresse IP publique de l'accès internet de la Semabox
def ip_publique_net():
    response = requests.get("https://ifconfig.me/ip")
    if response.status_code == 200:
        return response.text
    else:
        return "Impossible de récupérer l'adresse IP Publique"


print(ip_publique_net())

# Fonction pour scanner le réseau et retourner un DataFrame Pandas avec les rés>
def scan_network():
    target = adresse_ip_recuperation()
    nm = nmap.PortScanner()
    nm.scan(target, arguments="-sS -O")
    rows = []
    for host in nm.all_hosts():
        hostname = nm[host].hostname()
        for proto in nm[host].all_protocols():
            lport = nm[host][proto].keys()
            lport = sorted(lport)
            for port in lport:
                state = nm[host][proto][port]['state']
                rows.append((host, hostname, proto, port, state))
    df = pd.DataFrame(rows, columns=["Host", "Hostname", "Protocol", "Port", "State"])
    return df
# Création de l'application Dash
app = dash.Dash(__name__) # Définition de la mise en page
app.layout = html.Div(children=[
    html.H1(children='Tableau de bord de l\'analyse de réseau'),
    html.Button('Scan', id='button-scan'),
    html.Div(id='output-table')
])

# Définition de la logique de l'application
@app.callback(
    dash.dependencies.Output('output-table', 'children'),
    [dash.dependencies.Input('button-scan', 'n_clicks')])
def update_output(n_clicks):
    if n_clicks is not None:
        df = scan_network()
        table = html.Table([
            html.Thead(
                html.Tr([
                    html.Th("Host"),
                    html.Th("Hostname"),
                    html.Th("Protocol"),
                    html.Th("Port"),
                    html.Th("State")
                ])
            ),
            html.Tbody([
		html.Tr([
                    html.Td(df.iloc[i]["Host"]),
                    html.Td(df.iloc[i]["Hostname"]),
                    html.Td(df.iloc[i]["Protocol"]),
                    html.Td(port),
                    html.Td(df.iloc[i]["State"])
                ]) for i, port in enumerate(df['Port'])
            ])
        ])
        return table

if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0')

