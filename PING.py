import subprocess
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

app = dash.Dash(__name__)

def ping(host):
    # Exécuter la commande ping en arrière-plan
    process = subprocess.Popen(['ping', '-c', '1', '-w', '1', host],
                               stdout=subprocess.PIPE, 
                               stderr=subprocess.PIPE)
    # Récupérer la sortie
    output, error = process.communicate()
    # Vérifier si la réponse est 0 pour savoir si le ping a réussi
    if process.returncode == 0:
        ping_output = output.decode('utf-8')
        # Extraire la latence du ping à partir de la sortie
        latency = ping_output.split("time=")[1].split(" ms")[0]
        return f"Le ping a réussi, latence du ping : {latency} ms"
    else:
        return "Le ping a échoué"

app.layout = html.Div([
    dcc.Input(id='input-box', type='text', value='1.1.1.1'),
    html.Button('Ping', id='button'),
    html.Div(id='output-container-button',
             children='Entrez une adresse IP et cliquez sur le bouton Ping')
])

@app.callback(
    Output('output-container-button', 'children'),
    Input('button', 'n_clicks'),
    Input('input-box', 'value'))
def update_output(n_clicks, value):
    if n_clicks is not None:
        return ping(value)

if __name__ == '__main__':
    app.run_server(debug=True)

