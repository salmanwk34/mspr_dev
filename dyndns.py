# Importation des modules Python nécessaires
import socket
import dns.query
import dns.update
import ifaddr

# Déclaration de nos variables
hostname = socket.gethostname()  # Pour récupérer le hostname


def get_net_address() -> str:  # Récupère les informations sur les interfaces réseau configurées sur le système
    interfaces = ifaddr.get_adapters()
    for interface in interfaces:  # Parcourt les interfaces pour trouver celle qui s'appelle "tun0" (vpn)
        if interface.nice_name == "tun0":  # On sélectionne la carte réseau en question (celle du vpn)
            for ip in interface.ips:  # Adresse IP de l'interface (celle qui apparaitra sur l'enregistrement dns)
                if ip.is_IPv4:
                    return ip.ip
            break


# Ajout d'un enregistrement DNS
def add_dns_record(domain, ip_dns, host, new_ip, enregistrement, ttl):
    # Créez un objet Update
    update = dns.update.Update(domain)
    update.add(host, ttl, enregistrement, new_ip)  # On ajoute l'enregistrement de l'hôte
    response = dns.query.tcp(update, ip_dns)  # Et on envoie la requête au dns (mis à jour)

    # On vérifie que la requête a aboutie
    if response.rcode() == 0:
        print("Enregistrement DNS ajouté avec succès")


def main():
    # Ajout de l'enregistrement DNS grâce à notre dns record
    add_dns_record(
        domain='cma4.box',  # le nom de domaine que l'on va attribuer
        ip_dns='192.168.2.1',  # serveur dns à qui faire la requête
        host=hostname,  # le nom d'hôte de notre machine
        new_ip=get_net_address(),  # l'adresse IP de la Semabox (celle du VPN)
        enregistrement='A',  # le type d'enregistrement de notre nouvel hôte
        ttl=300,  # durée d'existence de la requête
    )


if __name__ == "__main__":
    main()
