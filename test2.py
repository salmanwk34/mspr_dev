import subprocess
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

app = dash.Dash(__name__)

def ping(host):
    # Exécuter la commande ping en arrière-plan
    process = subprocess.Popen(['ping', '-c', '1', '-w', '1', host],
                               stdout=subprocess.PIPE, 
                               stderr=subprocess.PIPE)
    # Récupérer la sortie
    output, error = process.communicate()
    # Vérifier si la réponse est 0 pour savoir si le ping a réussi
    if process.returncode == 0:
        ping_output = output.decode('utf-8')
        # Extraire la latence du ping à partir de la sortie
        latency = ping_output.split("time=")[1].split(" ms")[0]
        return f"Le ping a réussi, latence du ping : {latency} ms"
    else:
        return "Le ping a échoué"

def adresse_ip_recuperation():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    local_ip = s.getsockname()[0]
    s.close()
    network = ipaddress.ip_network(f"{local_ip}/24", strict=False)
    return str(network)

def scan_network():
    target = adresse_ip_recuperation()
    nm = nmap.PortScanner()
    nm.scan(target, arguments="-sS -O")
    rows = []
    for host in nm.all_hosts():
        hostname = nm[host].hostname()
        for proto in nm[host].all_protocols():
            lport = nm[host][proto].keys()
            lport = sorted(lport)
            for port in lport:
                state = nm[host][proto][port]['state']
                rows.append((host, hostname, proto, port, state))
    df = pd.DataFrame(rows, columns=["Host", "Hostname", "Protocol", "Port", "State"])
    return df

app.layout = html.Div([
    html.H1(children='Tableau de bord de l\'analyse de réseau'),
    html.Button('Scan', id='button-scan'),
    html.Div(id='output-table'),
    dcc.Input(id='input-box', type='text', value='1.1.1.1'),
    html.Button('Ping', id='button-ping'),
    html.Div(id='output-container-button',
             children='Entrez une adresse IP et cliquez sur le bouton Ping')
])

@app.callback(
    Output('output-table', 'children'),
    Input('button-scan', 'n_clicks'))
def update_output_scan(n_clicks):
    if n_clicks is not None:
        df = scan_network()
        table = html.Table([
            html.Thead(
                html.Tr([
                    html.Th("Host"),
                    html.Th("Hostname"),
                    html.Th("Protocol"),
                    html.Th("Port"),
                    html.Th("State")
                ])
            ),
            html.Tbody([
                html.Tr([
                    html.Td(df.iloc[i]["Host"]),
                    html.Td(df.iloc[i]["Hostname"]),
                    html.Td(df.iloc[i]["Protocol"]),
                    html.Td(port),
                    html.Td(df.iloc[i]["State"])
                ]) for i, port in enumerate(df['Port'])
            ])
        ])
        return table
    else:
        return ''

@app.callback(
    Output('output-container-button', 'children'),
    Input('button-ping', 'n_clicks'),
    Input('input-box', 'value'))
def update_output_ping(n_clicks, value):
    if n_clicks is not None:
        return ping(value)

if __name__ == '__main__':
    app.run_server(debug=True)
